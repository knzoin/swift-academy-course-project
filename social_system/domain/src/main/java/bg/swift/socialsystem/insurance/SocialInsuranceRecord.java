package bg.swift.socialsystem.insurance;

import javax.persistence.*;
import java.time.Month;
import java.time.Year;
import java.time.YearMonth;

@Entity
public class SocialInsuranceRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    private double amount;

    @Convert(
            converter = YearMonthDateAttributeConverter.class
    )
    private YearMonth date;

    public SocialInsuranceRecord(double amount, Year year, Month month) {
        this.amount = amount;
        this.date = YearMonth.of(year.getValue(), month);
    }

    public SocialInsuranceRecord() {

    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public YearMonth getDate() {
        return date;
    }

    public void setDate(YearMonth date) {
        this.date = date;
    }
}
