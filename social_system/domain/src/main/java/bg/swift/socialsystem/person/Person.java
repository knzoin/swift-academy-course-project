package bg.swift.socialsystem.person;

import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String firstName;
    private String lastName;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private int height;
    private LocalDate dateOfBirth;
    private String address;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<Education> educations = new HashSet<>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<SocialInsuranceRecord> socialInsuranceRecords = new HashSet<>();

    public Person() {
    }

    public Person(String firstName, String lastName, char gender, int height, LocalDate dateOfBirth, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        if (gender == 'M') {
            this.gender = Gender.MALE;
        }
        if (gender == 'F') {
            this.gender = Gender.FEMALE;
        }
        this.height = height;
        this.dateOfBirth = dateOfBirth;
        this.address = address;
        this.educations = educations;
    }

    public void addInsuranceRecord(SocialInsuranceRecord record) {
        socialInsuranceRecords.add(record);
    }

    public Set<SocialInsuranceRecord> getSocialInsuranceRecords() {
        return socialInsuranceRecords;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Set<Education> getEducations() {
        return educations;
    }

    public void setEducations(Set<Education> educations) {
        this.educations = educations;
    }

    public void setSocialInsuranceRecords(Set<SocialInsuranceRecord> socialInsuranceRecords) {
        this.socialInsuranceRecords = socialInsuranceRecords;
    }

}


